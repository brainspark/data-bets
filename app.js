'use strict';

var config = require('./config');
var mongoose = require('mongoose');
var express = require('express');
var bodyParser = require('body-parser');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var MongoStore = require('connect-mongo')(session);
var favicon = require('serve-favicon');
var log = require('./libs/log')(module);

module.exports = function (app, passport) {
    /**
     * Page Rendering
     * */
    app.set('views', __dirname + '/views');
    app.set('view engine', 'twig');
    app.set("twig options", {
        strict_variables: false
    });

    /**
     * Public directory
     * */
    app.use(express.static(__dirname + '/public'));

    /**
     * Favicon
     * */
    app.use(favicon(__dirname + '/public/favicon.png'));

    /**
     * Sessions
     * */
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(cookieParser());
    app.use(session({
        secret: config.get('session:secret'),
        key: config.get('session:key'),
        cookie: config.get('session:cookie'),
        resave: true,
        saveUninitialized: true,
        store: new MongoStore({mongooseConnection: mongoose.connection})
    }));

    // use passport session
    app.use(passport.initialize());
    app.use(passport.session());
};
