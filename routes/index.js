'use strict';

var config = require('../config');
var checkAuth = require('../middleware/checkAuth');
var pages = require('../controllers/pages');
var api = require('../controllers/api');
var HttpError = require('../error').HttpError;

module.exports = function (app, passport) {


    /**
     * Pages
     */
    //index
    app.get('/', checkAuth, pages.home);

    //login
    app.get('/sign-in', checkAuth, pages.signIn);
    app.get('/sign-up', checkAuth, pages.signUp);
    app.get('/verify-email/:token', pages.verifyEmail);
    app.get('/forgot-password', checkAuth, pages.forgotPassword);
    app.get('/reset-password/:token', pages.resetPassword);
    app.get('/logout', pages.logout);


    /**
     * API
     */
    app.post('/api/sign-in', api.signIn);
    app.post('/api/sign-up', api.signUp);
    app.post('/api/forgot-password', api.forgotPassword);
    app.post('/api/reset-password/:token', api.resetPassword);
    //app.post('/api/reset-password', api.resetPassword);


    /**
     * Default
     */
    app.get('*', function (req, res, next) {
        res.status(404);

        // respond with html page
        if (req.accepts('html')) {
            return pages.page404(req, res);
        }

        // respond with json
        if (req.accepts('json')) {
            return api.page404(req.res);
        }

        // default to plain-text
        res.type('txt').send('Not found');

        //return next( new HttpError(404, "Page not found!") );
    });
};