var config = require('./config'),
    mongoose = require('./libs/mongoose'),
    async = require('async'),
    User = require('./models/user'),
    log = require('./libs/log')(module);

function openConnection(cb) {
    mongoose.createConnection(config.get('db:connection'), function (err) {
        if(err) throw err;

        console.log('connected to database ' + config.get('db:name'));
        cb();
    });
}

function dropDatabase(cb) {
    var db = mongoose.connection.db;
    db.dropDatabase(function () {
        console.log('dropped database ' + config.get('db:name'));
        cb();
    });
}

function createBaseUser(cb) {
    var admin = new User({
        name: config.get('project:admin:administrator'),
        username: config.get('project:admin:username'),
        email: config.get('project:admin:email'),
        password: config.get('project:admin:password')
    });
    admin.save(function () {
        console.log('created database ' + config.get('db:name'));
        console.log('created base admin user');
        cb();
    });
}

function ensureIndexes(cb) {
    async.each(Object.keys(mongoose.models), function (model, callback) {
        mongoose.models[model].ensureIndexes(callback);
    }, function () {
        console.log('indexes ensured completely');
        cb();
    });
}

function closeConnection() {
    mongoose.disconnect();
    console.log('disconnected');
}

async.series(
    [
        openConnection,
        dropDatabase,

        createBaseUser,

        ensureIndexes
    ],
    closeConnection
);