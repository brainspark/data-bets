var url = 'https://betegy.com/';
var email = 'twinsdbv@gmail.com';
var password = '14061993';
var form = 'form[name="signin"]';
var button = 'button[type="submit"]';
var waitForElement = '.status-euro2016';
var AjaxData = [];

var casper = require('casper').create({
    verbose: true,
    logLevel: 'debug',
    userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22',
    pageSettings: {
        loadImages:  false,         // The WebPage instance used by Casper will
        loadPlugins: false         // use these settings
    },
    viewportSize: {
        width: 1280,
        height: 800
    }
});

// print out all the messages in the headless browser context
casper.on('remote.message', function(msg) {
    this.echo('remote message caught: ' + msg);
});

// print out all the messages in the headless browser context
casper.on("page.error", function(msg, trace) {
    this.echo("Page Error: " + msg, "ERROR");
});

casper.on("resource.received", function(resource){
    // somehow identify this request, here: if it contains ".json"
    // it also also only does something when the stage is "end" otherwise this would be executed two times
    if (resource.url.indexOf("algorithm_recommendation") != -1 && resource.stage == "end") {
        var data = casper.evaluate(function(url){
            // synchronous GET request
            return __utils__.sendAJAX(url, "GET");
        }, resource.url);

        AjaxData.push( data );
        // do something with data, you might need to JSON.parse(data)
    }
});

casper.start(url, function() {
    this.fill(form, {
            email: email,
            password: password
        },
        false);

    this.click(button);
    this.waitForSelector(waitForElement);
});

//check login status
//casper.then(function() {
//    this.captureSelector('capture.jpg', 'html');
//});

casper.then(function () {
    this.evaluate(function () {
        console.log(document.querySelector('.status-euro2016').innerHTML);
    });
});

casper.thenOpen('https://betegy.com/picks', function(){

});

casper.run( function() {
    this.exit();
});