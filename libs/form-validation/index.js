var validator = require('validator');

var data;

module.exports = {

    check: function (options, callback) {
            data = options.data;
            var validate = options.fields;


        for (var i=0; i < validate.length; i++) {
            if (this[validate[i]]) {
                var res = this[validate[i]](data[validate[i]]);

                if(res.result == 'failed') return callback(res);
            } else {
                console.log('Method "' + [validate[i]] + '" of validation doesn\'t exist!');
                return callback({result: 'failed', description: 'Sorry but try later'});
            }
        }

        callback(null);
    },

    email: function (email) {
        if( !email ) return {result: 'failed', description: 'Email can not be empty'};
        if( !validator.isEmail(email) ) return {result: 'failed', description: 'Email must be valid'};

        return {result: 'success'}
    },
    
    password: function (password) {
        if( !password ) return {result: 'failed', description: 'Password can not be empty'};
        if( password.length < 3) return {result: 'failed', description: 'Password should be equal to or greater than 3 characters'};

        return {result: 'success'}
    },

    password_confirm: function (password_confirm) {
        if( !password_confirm ) return {result: 'failed', description: 'Password Confirm can not be empty'};
        if( data.password != password_confirm) return {result: 'failed', description: 'Password Confirm and Password are not equal'};

        return {result: 'success'}
    },

    first_name: function (first_name) {
        if( !first_name ) return {result: 'failed', description: 'First Name can not be empty'};
        if( first_name.length < 3) return {result: 'failed', description: 'First Name should be equal to or greater than 3 characters'};

        return {result: 'success'}
    },

    last_name: function (last_name) {
        if( !last_name ) return {result: 'failed', description: 'Last Name can not be empty'};
        if( last_name.length < 3) return {result: 'failed', description: 'Last Name should be equal to or greater than 3 characters'};

        return {result: 'success'}
    }
};