
/**
 * Module dependencies.
 */
var template = require('./templates');
var transport = require('./transport');
var async = require('async');

/**
 * Expose
 */

module.exports = {

    sendOne: function(options, callback){
        async.waterfall([

            function(callback) {
                transport.get(options.transport, function(err, transport) {
                    callback(err, transport);
                });
            },

            function (transport, callback) {
                template.get(options, function (err, template) {
                    callback(err, transport, template);
                });
            },

            function(transport, template, callback) {
                transport.sendMail(template, function (err, result) {
                    callback(err, result);
                })
            }
        ], callback);
    }
};
