var config = require('../../config');

module.exports = {
    get: function(options, callback) {
        if(!options.email) return console.log('Email must have');
        if(!options.template) return console.log('Template must have');

        var template = config.get('mailer:templates:' + options.template);
        if(!template) return console.log('Mail template does not exist');

        options.host = config.get('project:host');
        template.to = options.email;
        template.from = config.get('mailer:mainMail');

        insertVariables(template.text, options, function (err, text) {
            if(err) callback(err);

            template.text = text;
            callback(null, template);
        });
    }
};

var insertVariables = function (text, options, callback) {
    var result = text.replace(/%(.*?)%/g, function (tag) {
            if (!options[tag.replace(/%/g, '')]) return console.log('Variable in template does not exist');

            return options[tag.replace(/%/g, '')];
    });

    callback(null, result);
};