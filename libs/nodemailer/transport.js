var config = require('../../config');
var nodemailer = require('nodemailer');

module.exports = {
    get: function (transportName, callback) {
        if(!transportName) transportName = 'Gmail';

        var transportOptions = {
            service: transportName,
            auth: {
                user: config.get('mailer:transport:'+ transportName +':username'),
                pass: config.get('mailer:transport:'+ transportName +':password')
            }
        };

        var transport = nodemailer.createTransport(transportOptions);

        callback(null, transport);
    }
};