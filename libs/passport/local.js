
/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var LocalStrategy = require('passport-local').Strategy;
var User = mongoose.model('User');
var log = require('../log')(module);

/**
 * Expose
 */

module.exports = new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function(email, password, callback) {
        var options = {
            criteria: { email: email },
            select: 'first_name last_name email hashedPassword salt'
        };
        User.load(options, function (err, user) {
            if (err) return callback(err);
            if (!user) {
                return callback({result: 'failed', description: 'Sorry, but user doesn\'t exist'});
            }
            if (!user.checkPassword(password)) {
                return callback({result: 'failed', description: 'Sorry, but the password is incorrect'});
            }
            return callback(null, user);
        });
    }
);
