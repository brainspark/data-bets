
/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var User = mongoose.model('User');

var local = require('./local');

/**
 * Expose
 */

module.exports = function (passport) {
    // serialize sessions
    passport.serializeUser(function(user, callback) {
        callback(null, user.id)
    });

    passport.deserializeUser(function(id, callback) {
        User.load({ criteria: { _id: id } }, function (err, user) {
            callback(err, user)
        })
    });

    // use these strategies
    passport.use(local);
};
