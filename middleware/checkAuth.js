module.exports =  function (req, res, next) {

    var userPages = ['/sign-in', '/sign-up', '/forgot-password'];

    if (in_array(req.url, userPages) && req.user) return res.redirect( '/' );
    if (!in_array(req.url, userPages) && !req.user) return res.redirect( '/sign-in' );

    next();
};


function in_array(value, array) {
    for(var i = 0; i < array.length; i++) {
        if(array[i] == value) return true;
    }
    return false;
}