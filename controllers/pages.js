'use strict';

var mongoose = require('mongoose');
var User = mongoose.model('User');


module.exports = {

    home: function(req, res) {
        res.render('app.html.twig', {
            title: 'Start page',
            first_name: req.user.first_name,
            last_name: req.user.last_name
        })
    },

    signIn: function(req, res){
        var apiMessage = req.session.apiMessage || '';
        req.session.apiMessage = null;

        res.render('login.html.twig', {
            title: "Sign In",
            namePage: 'sign-in.html.twig',
            apiMessage: apiMessage
        });
    },

    signUp: function(req, res){
        res.render('login.html.twig', {
            title: "Sign Up",
            namePage: 'sign-up.html.twig'
        });
    },

    verifyEmail: function (req, res) {
        User.findByToken(req.params.token, Date.now(), function(err, user) {

            if (!user) {
                req.session.apiMessage = {result: 'failed', description: 'Sorry, but token is invalid or has expired'};
                return res.redirect('/forgot-password');
            }

            user.token = {
                id: null,
                expires: null
            };
            user.status = true;

            user.save( function(err) {
                if(err) return console.log(err);

                req.session.apiMessage = {result: 'success', description: 'Thx, confirm success! Now you can enter with your login and password'};
                res.redirect('/sign-in');
            });

        });
    },

    forgotPassword: function (req, res) {
        var apiMessage = req.session.apiMessage || '';
        req.session.apiMessage = null;

        res.render('login.html.twig', {
            title: "Forgot Password?",
            namePage: 'forgot-password.html.twig',
            apiMessage: apiMessage
        });
    },

    resetPassword: function (req, res) {
        User.findByToken(req.params.token, Date.now(), function(err, user) {
            if (!user) {
                req.session.apiMessage = {result: 'failed', description: 'Sorry, but token is invalid or has expired. Try again.'};
                return res.redirect('/forgot-password');
            }

            res.render('login.html.twig', {
                title: "Reset Password",
                namePage: 'reset-password.html.twig'
            });
        });
    },

    logout: function (req, res) {
        req.logout();
        res.redirect('/');
    },
    
    page404: function (req, res) {
        res.render('404.html.twig', {
            title: 'Page not found'
        });
    }
};