'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var User = mongoose.model('User');
var passport = require('passport');
var async = require('async');
var pages = require('./pages');
var crypto = require('crypto');
var nodemailer = require('../libs/nodemailer');
var log = require('../libs/log')(module);
var FormValidation = require('../libs/form-validation');


module.exports = {

    /**
     * Auth user
     */
    signIn: function (req, res, next) {
        FormValidation.check({
            data: req.body,
            fields: ['email', 'password']

        }, function (err) {
            if(err) return res.json(err);

            User.findByEmail(req.body.email, function (err, user) {
                if(user && !user.status) {
                    return res.json({result: 'failed', description: 'Sorry, but you must confirm your email address. Please, check your email'});
                } else {

                    passport.authenticate('local', function(err, user) {
                        if (err) return res.json(err);

                        req.logIn(user, function(err) {
                            if (err) return next(err);

                            return res.json({result: 'success'});
                        });
                    })(req, res, next);
                }
            });
        });
    },

    /**
     * Create user
     */
    signUp: function (req, res) {
        FormValidation.check({
            data: req.body,
            fields: ['first_name', 'last_name', 'email', 'password', 'password_confirm']

        }, function (err) {
            if(err) return res.json(err);
            
            User.findByEmail(req.body.email, function (err, user) {
                if(user) {
                    return res.json({result: 'failed', description: 'Sorry, but this email is already in use'});
                } else {

                    user = new User(req.body);
                    user.provider = ['local'];

                    async.waterfall([

                        function(callback) {
                            crypto.randomBytes(20, function(err, buf) {
                                var token = buf.toString('hex');
                                callback(err, token);
                            });
                        },

                        function(token, callback) {
                            user.token = {
                                id: token,
                                expires: Date.now() + 3600000 // 1 hour
                            };

                            user.save(function(err) {
                                callback(err, token, user);
                            });
                        },

                        function(token, user, callback) {
                            nodemailer.sendOne({
                                email: user.email,
                                token: token,
                                template: 'verifyEmail'
                            }, function(err) {
                                if(err) return callback(err);

                                console.log('An e-mail has been sent to ' + user.email + ' with further instructions.');
                                callback(err, 'done');
                            });
                        }

                    ], function(err) {
                        if (err) return log.error(err);

                        res.json({result: 'success', description: 'You must confirm your email address. Please, check your email'});
                    });
                }
            });
        })
    },


    /**
     * Forgot password
     */
    forgotPassword: function(req, res) {
        FormValidation.check({
            data: req.body,
            fields: ['email']

        }, function (err) {
            if(err) return res.json(err);

            User.findByEmail(req.body.email, function (err, user) {
                if(!user) {
                    return res.json({result: 'failed', description: 'Sorry, but this email does not exist in database. Please, register.'});
                } else {

                    async.waterfall([

                        function(callback) {
                            crypto.randomBytes(20, function(err, buf) {
                                var token = buf.toString('hex');
                                callback(err, token);
                            });
                        },

                        function(token, callback) {
                            user.token = {
                                id: token,
                                expires: Date.now() + 3600000 // 1 hour
                            };

                            user.save(function(err) {
                                callback(err, token, user);
                            });
                        },

                        function(token, user, callback) {
                            nodemailer.sendOne({
                                email: user.email,
                                token: token,
                                template: 'passwordReset'
                            }, function(err) {
                                if(err) return callback(err);

                                console.log('An e-mail has been sent to ' + user.email + ' with further instructions.');
                                callback(err, 'done');
                            });
                        }

                    ], function(err) {
                        if (err) return log.error(err);

                        res.json({result: 'success', description: 'Please, check your email with further instructions.'});
                    });
                }
            });
        });
    },


    /**
     * Reset password
     */

    resetPassword: function (req, res) {
        async.waterfall([
            function(callback) {
                User.findByToken(req.params.token, Date.now(), function(err, user) {
                    if (!user) {
                        callback({result: 'failed', description: 'Sorry, but token is invalid or has expired. Try again.'});
                    }

                    FormValidation.check({
                        data: req.body,
                        fields: ['password', 'password_confirm']

                    }, function (err) {
                        if(err) return callback(err);

                        user.password = req.body.password;
                        user.status = true;
                        user.token = {
                            id: null,
                            expires: null
                        };

                        user.save(function(err) {
                            callback(err, user);
                        });

                    });
                });
            },
            function(user, callback) {

                nodemailer.sendOne({
                    email: user.email,
                    template: 'passwordChanged'
                }, function(err) {
                    console.log('Password of '+ user.email +' has been changed.');
                    callback(err, 'done');
                });
            }
        ], function(err) {
            if (err) {
                console.log(err);
                return res.json(err);
            }

            req.session.apiMessage = {result: 'success', description: 'Your password has been changed'};
            return res.json( {result: 'success'} );
        });
    },


    page404: function (req, res) {
        return res.json({result: 'failed', description: 'Page not found'})
    }
};