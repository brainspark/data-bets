var Helper = {

    init: function () {
        Helper.getYearsOfDev()
    },

    redirectTo: function (route) {
        location = location.origin + route;
    },

    getYearsOfDev: function () {
        $('.year-of-dev').html('2015 - ' + new Date().getFullYear());
    },

    clearForm: function (element) {
        $(element).find("input, textarea").val('');
    },

    submitBtnLoad: function (form, action) {
        action = action || 'on';
        var $submitBtn = $(form).find('button[type="submit"]');

        if(action == 'on') {
            $submitBtn
                .addClass('load')
                .prop('disabled', true);
        } else {
            $submitBtn
                .removeClass('load')
                .prop('disabled', false);
        }
    },

    showModalMessage: function (message) {
        var $modalWindow = $('#myModal'),
            $modalContent = $modalWindow.find('.modal-body p');

        $modalContent.html(message);
        $modalWindow.modal('show');
    },

    initSocialSigIn: function () {
        var $socialSignIn = $('#socialSignIn'),
            $submitBtn = $('form').find('button[type="submit"]');

        $socialSignIn.find('li').on('click', function () {
            $socialSignIn.addClass('load');
            $submitBtn.prop('disabled', true);

            //if the page is not reloaded
            setTimeout(function () {
                $socialSignIn.removeClass('load');
                $submitBtn.prop('disabled', false);
                window.stop();
                Helper.showModalMessage('Sorry, but try later');
            }, 10000);
        });
    },

    initLoginPage: function () {
        Helper.initSocialSigIn();

        $('#signInForm').submit(function(e){
            e.stopPropagation();
            e.preventDefault();

            var data = $(this).serialize(),
                formID = '#' + $(this).attr('id'),
                action = $(this).data('action');

            $.ajax({
                type: "POST",
                url: action,
                data: data,
                beforeSend: function () {
                    Helper.submitBtnLoad(formID);
                },
                success: function(response){
                    if(response.result == 'failed' && response.description) Helper.showModalMessage(response.description);
                    if(response.result == 'success') Helper.redirectTo('/');
                },
                complete: function () {
                    Helper.submitBtnLoad(formID, 'off');
                }
            });
        });


        $('#signUpForm').submit(function(e){
            e.stopPropagation();
            e.preventDefault();

            var data = $(this).serialize(),
                formID = '#' + $(this).attr('id'),
                action = $(this).data('action');

            $.ajax({
                type: "POST",
                url: action,
                data: data,
                beforeSend: function () {
                    Helper.submitBtnLoad(formID);
                },
                success: function(response){
                    if(response.description) {
                        Helper.showModalMessage(response.description);
                        if(response.result == 'success') Helper.clearForm(formID);
                    }
                },
                complete: function () {
                    Helper.submitBtnLoad(formID, 'off');
                }
            });
        });

        $('#forgotPassword').submit(function(e){
            e.stopPropagation();
            e.preventDefault();

            var data = $(this).serialize(),
                formID = '#' + $(this).attr('id'),
                action = $(this).data('action');

            $.ajax({
                type: "POST",
                url: action,
                data: data,
                beforeSend: function () {
                    Helper.submitBtnLoad(formID);
                },
                success: function(response){
                    if(response.description) {
                        Helper.showModalMessage(response.description);
                        if(response.result == 'success') Helper.clearForm(formID);
                    }
                },
                complete: function () {
                    Helper.submitBtnLoad(formID, 'off');
                }
            });
        });

        $('#resetPassword').submit(function(e){
            e.stopPropagation();
            e.preventDefault();

            var data = $(this).serialize(),
                formID = '#' + $(this).attr('id'),
                url = '/api' + location.pathname;

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                beforeSend: function () {
                    Helper.submitBtnLoad(formID);
                },
                success: function(response){
                    if(response.description) Helper.showModalMessage(response.description);

                    if(response.result == 'success') {
                        Helper.clearForm(formID);
                        Helper.redirectTo('/sign-in');
                    }
                },
                complete: function () {
                    Helper.submitBtnLoad(formID, 'off');
                }
            });
        });
    },

    checkAPIMessage: function (apiMessage) {
        if(apiMessage.description) Helper.showModalMessage(apiMessage.description);
    }
};